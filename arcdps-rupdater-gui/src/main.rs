#![windows_subsystem = "windows"]

extern crate arcdps_rupdater;
extern crate failure;
extern crate gettextrs;
extern crate gio;
extern crate glib;
extern crate gtk;
extern crate open;

mod app;
mod error;
mod globals;
mod ui;
mod ui_builder;
mod util;

use std::env;

use gettextrs::gettext as g_;
use gio::{resources_register, Resource};

use app::App;

fn main() {
    match env::current_exe() {
        Ok(program_path) => {
            arcdps_rupdater::setup_textdomain(program_path);
        }
        Err(e) => eprintln!(
            "impossible to load textdomain, skipping localization features: {}",
            e
        ),
    }
    let resource_bytes = include_bytes!("../../share/org.nurupoga.ArcdpsRupdater.gresource");
    let resource = Resource::new_from_data(&glib::Bytes::from(resource_bytes.as_ref()));
    if let Err(ref err) = resource {
        eprintln!("{}: {}", g_("error while loading GIO resource file"), err);
    }
    let resource = resource.expect("error while unwrapping resource");
    resources_register(&resource);
    App::new().run();
}
