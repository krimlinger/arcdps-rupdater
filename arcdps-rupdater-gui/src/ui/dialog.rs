use arcdps_rupdater::error::format_causes;
use gtk::{self, *};

use error::Error;
use ui::UI;

struct ErrorDialog(gtk::MessageDialog);

struct InfoDialog(gtk::MessageDialog);

pub struct TextEntryDialog {
    pub entry: gtk::Entry,
    pub dialog: gtk::MessageDialog,
}

impl ErrorDialog {
    fn new<W: IsA<gtk::Window>>(parent: &W, error: &Error) -> ErrorDialog {
        let mut dialog_flags = gtk::DialogFlags::MODAL;
        dialog_flags.insert(gtk::DialogFlags::DESTROY_WITH_PARENT);
        let error_message = format_causes(error);

        ErrorDialog(gtk::MessageDialog::new(
            Some(parent),
            dialog_flags,
            gtk::MessageType::Error,
            gtk::ButtonsType::Ok,
            &error_message,
        ))
    }

    fn run(&self) -> i32 {
        self.0.run()
    }
}

impl Drop for ErrorDialog {
    fn drop(&mut self) {
        self.0.destroy();
    }
}

impl InfoDialog {
    fn new<W: IsA<gtk::Window>>(parent: &W, info: &str) -> InfoDialog {
        let mut dialog_flags = gtk::DialogFlags::MODAL;
        dialog_flags.insert(gtk::DialogFlags::DESTROY_WITH_PARENT);

        InfoDialog(gtk::MessageDialog::new(
            Some(parent),
            dialog_flags,
            gtk::MessageType::Info,
            gtk::ButtonsType::Ok,
            info,
        ))
    }

    fn run(&self) -> i32 {
        self.0.run()
    }
}

impl Drop for InfoDialog {
    fn drop(&mut self) {
        self.0.destroy();
    }
}

impl TextEntryDialog {
    pub fn new<W: IsA<gtk::Window>>(
        parent: &W,
        msg: &str,
        max_length: Option<i32>,
    ) -> TextEntryDialog {
        let mut dialog_flags = gtk::DialogFlags::MODAL;
        dialog_flags.insert(gtk::DialogFlags::DESTROY_WITH_PARENT);

        let dialog = gtk::MessageDialog::new(
            Some(parent),
            dialog_flags,
            gtk::MessageType::Question,
            gtk::ButtonsType::OkCancel,
            msg,
        );
        let entry = gtk::Entry::new();
        if let Some(max_length) = max_length {
            entry.set_max_length(max_length);
        }
        let dialog_box = dialog.get_content_area();
        dialog_box.pack_end(&entry, false, false, 0);

        TextEntryDialog { entry, dialog }
    }

    pub fn run(&self) -> i32 {
        self.dialog.show_all();
        self.dialog.run()
    }
}

impl Drop for TextEntryDialog {
    fn drop(&mut self) {
        self.dialog.destroy();
    }
}

impl UI {
    pub fn info_notifier(&self) -> impl Fn(&str) {
        let main_window = self.main_window.clone();
        move |info| {
            println!("{}", info);
            InfoDialog::new(&main_window, info).run();
        }
    }

    pub fn error_notifier(&self) -> impl Fn(&Error) {
        let main_window = self.main_window.clone();
        move |err| {
            eprintln!("{}", err);
            ErrorDialog::new(&main_window, &err).run();
        }
    }

    pub fn text_entry_asker(&self) -> impl Fn(&str, Option<i32>) -> TextEntryDialog {
        let main_window = self.main_window.clone();
        move |msg, max_length| TextEntryDialog::new(&main_window, msg, max_length)
    }
}
