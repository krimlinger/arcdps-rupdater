mod content;
mod dialog;

use std::path::PathBuf;
use std::rc::Rc;
use std::sync::mpsc::{self, TryRecvError};
use std::sync::{Arc, Mutex};
use std::thread;

use arcdps_rupdater::config::{self, Config};
use arcdps_rupdater::error::format_causes;
use arcdps_rupdater::hash;
use arcdps_rupdater::updater::{self, execute_gw2, Status};
use failure::Fail;
use gettextrs::gettext as g_;
use gio::*;
use gtk::{self, *};
use open;

use globals;
use ui::content::Content;
use ui_builder::UIBuilder;
use util;

#[derive(Clone)]
struct UIConfigBuilder {
    gw2_dir_entry: gtk::Entry,
    gw2_exec_check_button: gtk::CheckButton,
    blacklist_list_box: gtk::ListBox,
    main_window: gtk::ApplicationWindow,
    last_config_sender: mpsc::Sender<Config>,
}

impl UIConfigBuilder {
    pub fn create_config(&self) -> Config {
        Config::new()
            .with_gw2_dir(self.gw2_dir_entry.get_text().unwrap_or("".to_string()))
            .with_gw2_exec(!self.gw2_exec_check_button.get_active())
            .with_blacklist(util::listbox_to_md5_hashset(&self.blacklist_list_box))
    }

    fn send_config_and_destroy_main_window(&self) {
        self.last_config_sender.send(self.create_config()).unwrap();
        self.main_window.destroy();
    }
}

pub struct UI {
    application: gtk::Application,
    main_window: gtk::ApplicationWindow,
    content: Content,
    // update_mutex contains a boolean used to bypass the
    // GW2 binary execution in case of a delete_event on the
    // main window (the boolean is then set to true).
    update_mutex: Arc<Mutex<bool>>,
    last_config_sender: mpsc::Sender<Config>,
    last_config_receiver: Arc<mpsc::Receiver<Config>>,
    gw2_exec_sender: mpsc::Sender<()>,
}

impl UI {
    pub fn new(ui_builder: &UIBuilder, gw2_exec_sender: mpsc::Sender<()>) -> UI {
        let application = ui_builder.get_application();
        let main_window = ui_builder.get_main_window();
        let content = Content::new(ui_builder);
        let update_mutex = Arc::new(Mutex::new(false));
        let (tc, rc) = mpsc::channel();

        UI {
            application,
            main_window,
            content,
            update_mutex,
            last_config_sender: tc,
            last_config_receiver: Arc::new(rc),
            gw2_exec_sender,
        }
    }

    fn create_uiconfigbuilder(&self) -> UIConfigBuilder {
        UIConfigBuilder {
            gw2_dir_entry: self.content.gw2_dir_entry.clone(),
            gw2_exec_check_button: self.content.gw2_exec_check_button.clone(),
            blacklist_list_box: self.content.blacklist_list_box.clone(),
            main_window: self.main_window.clone(),
            last_config_sender: self.last_config_sender.clone(),
        }
    }

    pub fn init(&self) {
        self.create_actions();
        self.connect_events();
    }

    pub fn create_actions(&self) {
        self.application.add_action(&self.content.about_action);
        self.application.add_action(&self.content.quit_action);
        self.application
            .set_accels_for_action("app.quit", &["<Ctrl>Q"]);
    }

    pub fn connect_events(&self) {
        self.connect_application_shutdown();
        self.connect_appmenu_about_activate();
        self.connect_appmenu_quit_activate(self.create_uiconfigbuilder());
        self.connect_window_delete_event(self.create_uiconfigbuilder());
        self.connect_blacklist_add_button_clicked();
        self.connect_blacklist_list_box_row_selected();
        self.connect_blacklist_remove_button_clicked();
        self.connect_blacklist_current_button_clicked();
        self.connect_update_button_clicked(self.create_uiconfigbuilder());
        self.connect_gw2_dir_file_chooser_file_set();
    }

    fn connect_application_shutdown(&self) {
        let update_mutex = self.update_mutex.clone();
        let notify_error = self.error_notifier();
        let last_config_receiver = self.last_config_receiver.clone();
        let gw2_exec_sender = self.gw2_exec_sender.clone();
        self.application.connect_shutdown(move |_| {
            let config = last_config_receiver.recv().unwrap();
            match config::write_to_filesystem(&config) {
                Ok(()) => println!("{}", g_("Configuration file updated.")),
                Err(err) => notify_error(&err.into()),
            }
            if update_mutex.try_lock().map(|x| *x).unwrap_or(false) {
                gw2_exec_sender.send(()).unwrap();
                return;
            }
            if config.gw2_exec {
                let gw2_dir = config.gw2_dir.clone();
                let gw2_exec_sender = gw2_exec_sender.clone();
                thread::spawn(move || {
                    execute_gw2(&gw2_dir).unwrap_or_else(|err| {
                        eprintln!(
                            "{}",
                            format_causes(&err.context(g_("GW2 binary execution failed")))
                        );
                    });
                    gw2_exec_sender.send(()).unwrap();
                });
            }
        });
    }

    fn connect_appmenu_about_activate(&self) {
        let main_window = self.main_window.clone();
        let notify_error = Rc::new(self.error_notifier());
        self.content.about_action.connect_activate(move |_, _| {
            let dialog = gtk::AboutDialog::new();
            dialog.set_logo_icon_name(globals::APP_ID);
            dialog.set_logo(None);
            dialog.set_program_name("Arcdps Rupdater");
            dialog.set_version(env!("CARGO_PKG_VERSION"));
            dialog.set_comments(g_("A simple arcdps DLLs updater for Guild Wars 2").as_str());
            dialog.set_website(env!("CARGO_PKG_HOMEPAGE"));
            dialog.set_website_label(g_("Arcdps Rupdater git repository").as_str());
            dialog.set_copyright(g_("© 2018 Ken Rimlinger").as_str());
            dialog.set_license_type(gtk::License::Gpl30);
            dialog.set_authors(&["Ken Rimlinger"]);
            dialog.set_translator_credits(g_("translator-credits").as_str());
            dialog.set_modal(true);
            dialog.set_transient_for(&main_window);
            if cfg!(target_os = "windows") {
                let notify_error = notify_error.clone();
                dialog.connect_activate_link(move |_, url| {
                    if let Err(e) = open::that(url) {
                        notify_error(&e.into());
                    }
                    Inhibit(true)
                });
            }
            dialog.run();
            dialog.destroy();
        });
    }

    fn connect_appmenu_quit_activate(&self, config_builder: UIConfigBuilder) {
        let update_mutex = self.update_mutex.clone();
        self.content.quit_action.connect_activate(move |_, _| {
            if let Ok(mut no_gw2_exec) = update_mutex.try_lock() {
                *no_gw2_exec = true;
                config_builder.send_config_and_destroy_main_window();
            }
        });
    }

    fn connect_window_delete_event(&self, config_builder: UIConfigBuilder) {
        let update_mutex = self.update_mutex.clone();
        self.main_window.connect_delete_event(move |_, _| {
            if let Ok(mut no_gw2_exec) = update_mutex.try_lock() {
                *no_gw2_exec = true;
                config_builder.send_config_and_destroy_main_window();
                return Inhibit(false);
            }
            Inhibit(true)
        });
    }

    fn connect_blacklist_list_box_row_selected(&self) {
        let blacklist_remove_button = self.content.blacklist_remove_button.clone();
        self.content
            .blacklist_list_box
            .connect_row_selected(move |_, _| {
                blacklist_remove_button.set_sensitive(true);
            });
    }

    fn connect_blacklist_add_button_clicked(&self) {
        let blacklist_list_box = self.content.blacklist_list_box.clone();
        let notify_error = self.error_notifier();
        let text_entry_dialog = self.text_entry_asker();
        self.content.blacklist_add_button.connect_clicked(move |_| {
            let dialog = text_entry_dialog(&g_("MD5 sum to blacklist: "), Some(32));
            if gtk::ResponseType::from(dialog.run()) == gtk::ResponseType::Ok {
                if let Some(text) = dialog.entry.get_text() {
                    if let Err(e) = hash::hex_str_to_digest(&text) {
                        notify_error(&e.into());
                    } else {
                        if !util::listbox_to_md5_hashset(&blacklist_list_box).contains(&text) {
                            util::add_label_to_listbox(&blacklist_list_box, &text);
                            blacklist_list_box.show_all();
                        }
                    }
                };
            }
        });
    }

    fn connect_blacklist_remove_button_clicked(&self) {
        let blacklist_list_box = self.content.blacklist_list_box.clone();
        self.content
            .blacklist_remove_button
            .connect_clicked(move |remove_button| {
                let selected_rows = blacklist_list_box.get_selected_rows();
                for row in selected_rows {
                    blacklist_list_box.remove(&row);
                }
                remove_button.set_sensitive(false);
            });
    }

    fn connect_blacklist_current_button_clicked(&self) {
        let blacklist_list_box = self.content.blacklist_list_box.clone();
        let notify_error = self.error_notifier();
        self.content
            .blacklist_current_button
            .connect_clicked(move |_| match updater::uninstall_arcdps() {
                Ok(md5) => {
                    if !util::listbox_to_md5_hashset(&blacklist_list_box).contains(&md5) {
                        util::add_label_to_listbox(&blacklist_list_box, &md5);
                        blacklist_list_box.show_all();
                    }
                }
                Err(err) => notify_error(&err.into()),
            });
    }

    fn connect_update_button_clicked(&self, config_builder: UIConfigBuilder) {
        let update_mutex = self.update_mutex.clone();
        let notify_error = Rc::new(self.error_notifier());
        let notify_info = Rc::new(self.info_notifier());
        self.content
            .update_button
            .connect_clicked(move |update_button| {
                let config = config_builder.create_config();
                let update_spinner = gtk::Spinner::new();
                let update_label = update_button
                    .get_child()
                    .expect("Update button has no label.");
                update_button.set_sensitive(false);
                update_button.remove(&update_label);
                update_button.add(&update_spinner);
                update_spinner.show();
                update_spinner.start();
                let (tx, rx) = mpsc::channel();
                let (ty, ry) = mpsc::channel();
                let arcdps_path = config.gw2_dir.join("bin64").clone();
                let blacklist = config.blacklist.clone();
                let update_mutex_clone = update_mutex.clone();
                // Locker thread.
                thread::spawn(move || {
                    let _x = update_mutex_clone.lock().unwrap();
                    ry.recv().unwrap();
                });
                thread::spawn(move || {
                    tx.send(updater::update_arcdps(&arcdps_path, &blacklist))
                        .unwrap();
                });
                let update_button = update_button.clone();
                let notify_error = notify_error.clone();
                let notify_info = notify_info.clone();
                let config_builder = config_builder.clone();
                timeout_add(100, move || {
                    let mut should_continue = Continue(false);
                    let mut set_sensitive = true;
                    match rx.try_recv() {
                        Ok(Ok(Status::NoUpdateRequired)) => {
                            notify_info(&g_("no arcdps update was needed."));
                            ty.send(false).unwrap();
                            set_sensitive = false;
                            config_builder.send_config_and_destroy_main_window();
                        }
                        Ok(Ok(Status::HasBeenUpdated)) => {
                            notify_info(&g_("arcdps has been updated."));
                            ty.send(false).unwrap();
                            set_sensitive = false;
                            config_builder.send_config_and_destroy_main_window();
                        }
                        Ok(Ok(Status::BlacklistedVersion)) => {
                            notify_info(&g_(
                                "current arcdps version has been blacklisted, skipping.",
                            ));
                            ty.send(false).unwrap();
                            set_sensitive = false;
                            config_builder.send_config_and_destroy_main_window();
                        }
                        Ok(Err(err)) => {
                            notify_error(&err.into());
                            ty.send(false).unwrap();
                        }
                        Err(TryRecvError::Empty) => {
                            set_sensitive = false;
                            should_continue = Continue(true);
                        }
                        Err(TryRecvError::Disconnected) => {
                            notify_error(&TryRecvError::Disconnected.into());
                            ty.send(false).unwrap();
                        }
                    };
                    if set_sensitive {
                        update_spinner.stop();
                        update_button.remove(&update_spinner);
                        update_button.add(&update_label);
                        update_button.set_sensitive(true);
                    }
                    should_continue
                });
            });
    }

    fn connect_gw2_dir_file_chooser_file_set(&self) {
        let gw2_dir_entry = self.content.gw2_dir_entry.clone();
        self.content
            .gw2_dir_file_chooser
            .connect_file_set(move |fc| {
                let updated_dir = fc.get_filename().unwrap_or(PathBuf::new());
                gw2_dir_entry.set_text(updated_dir.to_str().unwrap_or(""));
            });
    }
}
