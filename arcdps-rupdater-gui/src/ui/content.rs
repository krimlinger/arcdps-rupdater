use std::result;

use arcdps_rupdater::config::{self, Config};
use gio;
use gtk::{self, *};

use error::Error;
use ui_builder;
use util;

pub struct Content {
    pub about_action: gio::SimpleAction,
    pub quit_action: gio::SimpleAction,
    pub gw2_dir_entry: gtk::Entry,
    pub gw2_dir_file_chooser: gtk::FileChooserButton,
    pub gw2_exec_check_button: gtk::CheckButton,
    pub blacklist_list_box: gtk::ListBox,
    pub blacklist_add_button: gtk::Button,
    pub blacklist_remove_button: gtk::Button,
    pub blacklist_current_button: gtk::Button,
    pub update_button: gtk::Button,
}

impl Content {
    pub fn new(ui_builder: &ui_builder::UIBuilder) -> Content {
        let about_action = gio::SimpleAction::new("about", None);
        let quit_action = gio::SimpleAction::new("quit", None);

        let config = config::read_from_filesystem()
            .or_else(|_| config::create_default_config())
            .or_else(|_| result::Result::Ok::<Config, &Error>(Config::new()))
            .expect("failed to create default configuration");

        let gw2_dir_entry = ui_builder.get_object::<gtk::Entry>("gw2_dir_entry");
        gw2_dir_entry.set_text(config.gw2_dir.to_str().unwrap_or(""));

        let gw2_dir_file_chooser =
            ui_builder.get_object::<gtk::FileChooserButton>("gw2_dir_file_chooser");
        gw2_dir_file_chooser.set_filename(&config.gw2_dir);

        let gw2_exec_check_button =
            ui_builder.get_object::<gtk::CheckButton>("gw2_exec_check_button");
        gw2_exec_check_button.set_active(!config.gw2_exec);

        let blacklist_list_box = ui_builder.get_object::<gtk::ListBox>("blacklist_list_box");
        for md5_sum in &config.blacklist {
            util::add_label_to_listbox(&blacklist_list_box, &md5_sum);
        }

        Content {
            about_action,
            quit_action,
            gw2_dir_entry,
            gw2_dir_file_chooser,
            gw2_exec_check_button,
            blacklist_list_box,
            blacklist_add_button: ui_builder.get_object::<gtk::Button>("blacklist_add_button"),
            blacklist_remove_button: ui_builder
                .get_object::<gtk::Button>("blacklist_remove_button"),
            blacklist_current_button: ui_builder
                .get_object::<gtk::Button>("blacklist_current_button"),
            update_button: ui_builder.get_object::<gtk::Button>("update_button"),
        }
    }
}
