use gtk::{self, *};

#[derive(Clone)]
pub struct UIBuilder(gtk::Builder);

impl UIBuilder {
    pub fn new() -> UIBuilder {
        let builder = gtk::Builder::new();

        builder
            .add_from_resource("/org/nurupoga/ArcdpsRupdater/ui/app_window.ui")
            .expect("failed to load \"app_window.ui\"");

        UIBuilder(builder)
    }

    pub fn get_application(&self) -> gtk::Application {
        self.0
            .get_application()
            .expect("failed to get the GTK Application from the GTK Builder")
    }

    pub fn get_main_window(&self) -> gtk::ApplicationWindow {
        self.get_object::<gtk::ApplicationWindow>("main_window")
    }

    pub fn get_object<T: IsA<gtk::Object>>(&self, name: &str) -> T {
        self.0
            .get_object(name)
            .expect(&format!("failed to get \"{}\" object from ui file", name))
    }
}
