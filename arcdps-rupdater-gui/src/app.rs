use gio::{self, ApplicationExt, ApplicationExtManual};
use glib;
use gtk::{self, *};

use std::sync::mpsc;

use globals;
use ui;
use ui_builder;

pub struct App {
    application: gtk::Application,
    gw2_exec_receiver: mpsc::Receiver<()>,
}

impl App {
    pub fn new() -> App {
        let application =
            gtk::Application::new(Some(globals::APP_ID), gio::ApplicationFlags::empty())
                .expect("failed to initialize GtkApplication");
        let (tx, rx) = mpsc::channel();

        application.connect_startup(move |app| {
            let ui_builder = ui_builder::UIBuilder::new();

            let main_window = ui_builder.get_main_window();
            main_window.set_application(app);
            main_window.set_icon_name(globals::APP_ID);

            let tx = tx.clone();
            ui::UI::new(&ui_builder, tx).init();

            glib::set_application_name("Arcdps Rupdater");
            glib::set_prgname(Some("Arcdps Rupdater"));

            app.connect_activate(move |_| {
                main_window.show_all();
                main_window.present();
            });
        });

        App {
            application,
            gw2_exec_receiver: rx,
        }
    }

    pub fn run(&self) -> i32 {
        let code = self.application.run(&[]);
        self.gw2_exec_receiver.recv().unwrap();
        code
    }
}
