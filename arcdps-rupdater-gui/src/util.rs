use std::collections::hash_map::RandomState;
use std::collections::hash_set::HashSet;

use glib::Cast;
use gtk::{self, BinExt, ContainerExt, LabelExt};

fn get_listboxrow_label_text_from_widget(widget: &gtk::Widget) -> Option<String> {
    widget
        .clone()
        .downcast::<gtk::ListBoxRow>()
        .expect("Widget to ListBoxRow downcast failed.")
        .get_child()
        .expect("ListBoxRow does not have a child.")
        .downcast::<gtk::Label>()
        .expect("Widget to Label downcast failed.")
        .get_text()
}

pub fn listbox_to_md5_hashset(listbox: &gtk::ListBox) -> HashSet<String, RandomState> {
    listbox
        .get_children()
        .iter()
        .map(|child| {
            let label = get_listboxrow_label_text_from_widget(&child);
            // Assume that the ListBoxRow label is a valid MD5 sum.
            label.expect("Missing ListBoxRow label.")
        })
        .collect()
}

pub fn add_label_to_listbox(listbox: &gtk::ListBox, text: &str) {
    let row = gtk::ListBoxRow::new();
    row.add(&gtk::Label::new(Some(text)));
    listbox.add(&row);
}
