use std::sync::mpsc::TryRecvError;
use std::{fmt, io, result};

use arcdps_rupdater::error::Error as ARError;
use arcdps_rupdater::error::{GenError, MD5Error};
use failure::{Backtrace, Fail};
use gettextrs::gettext as g_;
use glib;

// We are using a newtype to avoid orphan instances
// issues with From<x> traits implementations.
#[derive(Debug)]
pub struct Error(GenError<ErrorKind>);

impl Fail for Error {
    fn cause(&self) -> Option<&Fail> {
        self.0.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.0.backtrace()
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl Error {
    pub fn new(kind: ErrorKind, failure: Option<Box<Fail>>) -> Error {
        Error(GenError::new(kind, failure))
    }

    #[allow(dead_code)]
    pub fn kind(&self) -> &ErrorKind {
        self.0.kind()
    }
}

impl From<ErrorKind> for Error {
    fn from(e: ErrorKind) -> Error {
        Error::new(e, None)
    }
}

#[allow(dead_code)]
pub type Result<T> = result::Result<T, Error>;

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ErrorKind {
    ARError,
    GUIInit,
    RunDefaultBrowser,
    UpdateThreadDisconnected,
    MD5Error,
}

impl fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ErrorKind::ARError => g_("arcdps-rupdater error"),
                ErrorKind::GUIInit => g_("GUI initialization error"),
                ErrorKind::RunDefaultBrowser => g_("default web browser execution error"),
                ErrorKind::UpdateThreadDisconnected => g_("update thread disconnection error"),
                ErrorKind::MD5Error => g_("MD5 Error"),
            }
        )
    }
}

impl From<ARError> for Error {
    fn from(e: ARError) -> Error {
        Error::new(ErrorKind::ARError, Some(Box::new(e)))
    }
}

impl From<glib::error::BoolError> for Error {
    fn from(e: glib::error::BoolError) -> Error {
        Error::new(ErrorKind::GUIInit, Some(Box::new(e)))
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::new(ErrorKind::RunDefaultBrowser, Some(Box::new(e)))
    }
}

impl From<TryRecvError> for Error {
    fn from(e: TryRecvError) -> Error {
        Error::new(ErrorKind::UpdateThreadDisconnected, Some(Box::new(e)))
    }
}

impl From<MD5Error> for Error {
    fn from(e: MD5Error) -> Error {
        Error::new(ErrorKind::MD5Error, Some(Box::new(e)))
    }
}
