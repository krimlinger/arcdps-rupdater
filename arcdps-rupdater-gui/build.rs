use std::process::Command;

fn main() {
    Command::new("glib-compile-resources")
        .args(&["--generate", "org.nurupoga.ArcdpsRupdater.gresource.xml"])
        .current_dir("../share")
        .status()
        .unwrap();
}
