use std::env;
use std::fs::File;
use std::io::Write;
use std::path::Path;

fn main() {
    let out_dir = env::var("OUT_DIR").expect("unset OUT_DIR variable");
    let localedir = env::var("ARCDPS_RUPDATER_LOCALEDIR")
        .unwrap_or(format!("{}", Path::new("..").join("po").display()));
    let dest_path = Path::new(&out_dir).join("build_globals.rs");
    let mut f = File::create(&dest_path).expect("cannot create build_globals.rs");
    let globals = format!(
        "
pub static LOCALEDIR: &'static str = \"{}\";
",
        localedir
    );
    f.write_all(&globals.into_bytes()[..])
        .expect("error while writing to build_globals.rs");
}
