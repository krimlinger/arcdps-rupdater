extern crate md5;

pub use self::md5::Digest;
use std::{u64, u8};

use error::MD5Error;

// Todo: use Result with custom error type.
pub fn hex_str_to_digest(hex: &str) -> Result<Digest, MD5Error> {
    if hex.len() != 32 {
        return Err(MD5Error::IncorrectHashSize);
    }
    let mut hash: [u8; 16] = [0; 16];
    for i in 0..16 {
        if let Ok(r) = u64::from_str_radix(&hex[(i * 2)..(i * 2 + 2)], 16) {
            if r > u8::MAX as u64 {
                return Err(MD5Error::DigitOverflow);
            }
            hash[i] = r as u8;
        } else {
            return Err(MD5Error::DigitParsingFailure);
        }
    }
    Ok(Digest(hash))
}

pub fn is_matching<T: AsRef<[u8]>>(data: T, digest: &Digest) -> bool {
    md5::compute(data) == *digest
}

pub fn md5_sum<T: AsRef<[u8]>>(data: T) -> String {
    format!("{:x}", md5::compute(data))
}
