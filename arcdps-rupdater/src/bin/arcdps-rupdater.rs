extern crate clap;
extern crate gettextrs;

extern crate arcdps_rupdater;

use std::{env, process};

use self::gettextrs::gettext as g_;
use clap::{App, Arg};

use arcdps_rupdater::error::format_causes;
use arcdps_rupdater::{config, hash, updater};

fn main() {
    match env::current_exe() {
        Ok(program_path) => {
            arcdps_rupdater::setup_textdomain(program_path);
        }
        Err(e) => eprintln!(
            "impossible to load textdomain, skipping localization features: {}",
            e
        ),
    }
    let matches = App::new("arcdps-rupdater")
        .version(env!("CARGO_PKG_VERSION"))
        .arg(
            Arg::with_name("version")
                .short("v")
                .long("version")
                .multiple(true)
                .help(&g_("Show program version and exit.")),
        )
        .arg(
            Arg::with_name("gw2-exec")
                .conflicts_with("gw2-no-exec")
                .short("e")
                .long("gw2-exec")
                .help(&g_("Run the GW2 launcher after updating arcdps.")),
        )
        .arg(
            Arg::with_name("gw2-no-exec")
                .short("n")
                .long("gw2-no-exec")
                .help(&g_("Do not run the GW2 launcher after updating arcdps.")),
        )
        .arg(
            Arg::with_name("blacklist")
                .short("b")
                .long("blacklist")
                .takes_value(true)
                .value_name("MD5_SUM")
                .help(&g_(
                    "Blacklist an arcdps version using its hexadecimal md5 sum.",
                )),
        )
        .arg(
            Arg::with_name("unblacklist")
                .short("u")
                .long("unblacklist")
                .takes_value(true)
                .value_name("MD5_SUM")
                .help(&g_("Remove an arcdps version from the blacklist.")),
        )
        .get_matches();
    if matches.is_present("version") {
        println!("arcdps-rupdater v{}", env!("CARGO_PKG_VERSION"));
        process::exit(0);
    }
    if matches.is_present("blacklist") || matches.is_present("unblacklist") {
        match config::read_from_filesystem().as_mut() {
            Err(e) => {
                eprintln!("{}", format_causes(e));
                process::exit(1);
            }
            Ok(cfg) => {
                let md5_sum = matches.value_of("blacklist").unwrap_or_else(|| {
                    matches
                        .value_of("unblacklist")
                        .expect("You need to either blacklist or unblacklist.")
                });
                if let Err(e) = hash::hex_str_to_digest(&md5_sum) {
                    eprintln!("{}", format_causes(&e));
                    process::exit(1);
                }
                if matches.is_present("blacklist") {
                    cfg.add_to_blacklist(md5_sum);
                } else {
                    cfg.remove_from_blacklist(md5_sum);
                }
                if let Err(e) = config::write_to_filesystem(cfg) {
                    eprintln!("{}", format_causes(&e));
                    process::exit(1);
                }
            }
        }
        process::exit(0);
    }
    let mut config = config::OptionConfig::new();
    config.gw2_exec = some_if_true(matches.is_present("gw2-exec"), true)
        .or(some_if_true(matches.is_present("gw2-no-exec"), false));
    if let Err(e) = updater::run_arcdps_rupdater(config) {
        eprintln!("{}", format_causes(&e));
        process::exit(1);
    }
}

fn some_if_true(is_true: bool, value: bool) -> Option<bool> {
    if is_true {
        Some(value)
    } else {
        None
    }
}
