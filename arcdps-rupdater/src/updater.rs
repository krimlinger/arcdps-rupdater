extern crate gettextrs;

use std::collections::hash_map::RandomState;
use std::collections::hash_set::HashSet;
use std::fs;
use std::path::{Path, PathBuf};
use std::process::{self, Command};

use self::gettextrs::gettext as g_;

use config::{self, OptionConfig};
use downloader;
use error::{Result, ResultExt};
use hash::{self, Digest};

pub enum Status {
    NoUpdateRequired,
    HasBeenUpdated,
    BlacklistedVersion,
}

pub fn run_arcdps_rupdater(optionconfig: OptionConfig) -> Result<()> {
    let mut config = config::read_from_filesystem()?;
    config.load_optionconfig(optionconfig);
    match update_arcdps(&config.gw2_dir.join("bin64"), &config.blacklist)? {
        Status::NoUpdateRequired => println!("{}", g_("no arcdps update was needed.")),
        Status::HasBeenUpdated => println!("{}", g_("arcdps has been updated.")),
        Status::BlacklistedVersion => println!(
            "{}",
            g_("current arcdps version has been blacklisted, skipping.")
        ),
    }
    if config.gw2_exec {
        execute_gw2(&config.gw2_dir)?;
    }
    Ok(())
}

pub fn update_arcdps(
    arcdps_path: &Path,
    blacklist: &HashSet<String, RandomState>,
) -> Result<Status> {
    let arcdps_md5 = downloader::get_content(downloader::ARCDPS_DLL_MD5)?;
    if blacklist.contains(&arcdps_md5[..32]) {
        return Ok(Status::BlacklistedVersion);
    }
    // In case of ill-formed md5 sum, use a "null" md5sum
    // which will force the update.
    let new_arcdps_md5 = hash::hex_str_to_digest(&arcdps_md5[..32]).unwrap_or(Digest([0; 16]));
    if !update_needed(arcdps_path, &new_arcdps_md5) {
        Ok(Status::NoUpdateRequired)
    } else {
        let arcdps_dll = downloader::get_binary_data(downloader::ARCDPS_DLL)?;
        let arcdps_build_templates =
            downloader::get_binary_data(downloader::ARCDPS_BUILD_TEMPLATES)?;
        let dlls = ["d3d9.dll", "d3d9_arcdps_buildtemplates.dll"];
        for (filename, data) in dlls.iter().zip([arcdps_dll, arcdps_build_templates].iter()) {
            fs::write(arcdps_path.join(filename), &data)
                .with_path_context(&arcdps_path.join(filename))?;
        }
        Ok(Status::HasBeenUpdated)
    }
}

pub fn execute_gw2(gw2_dir: &Path) -> Result<()> {
    process::exit(
        Command::new(gw2_dir.join("Gw2-64.exe"))
            .current_dir(gw2_dir)
            .output()?
            .status
            .code()
            .unwrap_or(1),
    );
}

fn update_needed(arcdps_path: &Path, new_arcdps_md5: &Digest) -> bool {
    fs::read(arcdps_path.join("d3d9.dll"))
        .and_then(|dll| Ok(!hash::is_matching(dll, new_arcdps_md5)))
        .unwrap_or(true)
}

pub fn uninstall_arcdps() -> Result<String> {
    let cfg = &config::read_from_filesystem()?;
    let dlls = ["d3d9.dll", "d3d9_arcdps_buildtemplates.dll"]
        .iter()
        .map(|p| cfg.gw2_dir.join("bin64").join(p))
        .collect::<Vec<PathBuf>>();
    let arcdps_dll = fs::read(&dlls[0]).with_path_context(&dlls[0])?;
    for p in dlls.iter() {
        // Todo: better error management, if any ?
        let _ = fs::remove_file(&p);
    }
    Ok(hash::md5_sum(&arcdps_dll))
}
