extern crate reqwest;

use self::reqwest::IntoUrl;
use error::Result;
use std::io::Read;

pub const ARCDPS_DLL: &'static str = "http://www.deltaconnected.com/arcdps/x64/d3d9.dll";
pub const ARCDPS_DLL_MD5: &'static str = "http://www.deltaconnected.com/arcdps/x64/d3d9.dll.md5sum";
pub const ARCDPS_BUILD_TEMPLATES: &'static str =
    "http://www.deltaconnected.com/arcdps/x64/buildtemplates/d3d9_arcdps_buildtemplates.dll";

pub fn get_binary_data<T: IntoUrl>(url: T) -> Result<Vec<u8>> {
    let mut data = Vec::new();
    reqwest::get(url)?.read_to_end(&mut data)?;
    Ok(data)
}

pub fn get_content<T: IntoUrl>(url: T) -> Result<String> {
    Ok(reqwest::get(url)?.text()?)
}
