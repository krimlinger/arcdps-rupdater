extern crate app_dirs2;
extern crate failure;
extern crate gettextrs;
extern crate reqwest;
extern crate toml;

use self::gettextrs::gettext as g_;

use std::path::{Path, PathBuf};
use std::{fmt, io, result};

use self::failure::{Backtrace, Fail};

#[derive(Debug)]
pub struct GenError<T>
where
    T: fmt::Display + fmt::Debug + Send + Sync + 'static,
{
    kind: T,
    failure: Option<Box<Fail>>,
}

impl<T> Fail for GenError<T>
where
    T: fmt::Display + fmt::Debug + Send + Sync + 'static,
{
    fn cause(&self) -> Option<&Fail> {
        self.failure.as_ref().map(|f| f.as_ref())
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        None
    }
}

impl<T> fmt::Display for GenError<T>
where
    T: fmt::Display + fmt::Debug + Send + Sync + 'static,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.kind, f)
    }
}

impl<T> GenError<T>
where
    T: fmt::Display + fmt::Debug + Send + Sync + 'static,
{
    pub fn new(kind: T, failure: Option<Box<Fail>>) -> GenError<T> {
        GenError { kind, failure }
    }

    pub fn kind(&self) -> &T {
        &self.kind
    }
}

impl<T> From<T> for GenError<T>
where
    T: fmt::Display + fmt::Debug + Send + Sync + 'static,
{
    fn from(e: T) -> GenError<T> {
        GenError::new(e, None)
    }
}

pub type Error = GenError<ErrorKind>;

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ErrorKind {
    ConfigDirectory,
    ConfigReading,
    ConfigWriting,
    HTTPRequest,
    IO,
    Generic(GenericError),
    IOWithPath(PathBuf),
    MD5(MD5Error),
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum MD5Error {
    DigitOverflow,
    DigitParsingFailure,
    IncorrectHashSize,
}

#[derive(Fail, Debug, Clone, Eq, PartialEq)]
#[fail(display = "{}", _0)]
pub struct GenericError(String);

impl Fail for MD5Error {
    fn cause(&self) -> Option<&Fail> {
        None
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        None
    }
}

impl fmt::Display for MD5Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                MD5Error::DigitOverflow => g_("MD5 hash contains some overflowing digits"),
                MD5Error::DigitParsingFailure => g_("MD5 hash contains impossible to parse digits"),
                MD5Error::IncorrectHashSize => {
                    g_("MD5 hashes should be a 32 bits hexadecimal string")
                }
            }
        )
    }
}

impl fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ErrorKind::ConfigDirectory => g_("configuration directory error"),
                ErrorKind::ConfigReading => g_("configuration file reading error"),
                ErrorKind::ConfigWriting => g_("configuration file writing error"),
                ErrorKind::HTTPRequest => g_("HTTP error"),
                ErrorKind::IO => g_("IO error"),
                ErrorKind::Generic(GenericError(msg)) => {
                    format!("{}: {}", g_("Generic error"), msg)
                }
                ErrorKind::IOWithPath(p) => {
                    format!("{} \"{}\"", g_("IO error while accessing"), p.display())
                }
                ErrorKind::MD5(e) => format!("{}", e),
            }
        )
    }
}

impl From<failure::Context<DisplayabalePath>> for Error {
    fn from(ctx: failure::Context<DisplayabalePath>) -> Error {
        if let Some(failure) = ctx.cause() {
            if let Some(e) = failure.downcast_ref::<io::Error>() {
                return Error::new(
                    ErrorKind::IOWithPath(ctx.get_context().0.clone()),
                    Some(Box::new(io::Error::new(e.kind(), format!("{}", e)))),
                );
            }
            return Error::new(
                ErrorKind::IOWithPath(ctx.get_context().0.clone()),
                Some(Box::new(GenericError(format!("{}", failure)))),
            );
        }
        return ErrorKind::IOWithPath(ctx.get_context().0.clone()).into();
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::new(ErrorKind::IO, Some(Box::new(e)))
    }
}

impl From<app_dirs2::AppDirsError> for Error {
    fn from(e: app_dirs2::AppDirsError) -> Error {
        Error::new(ErrorKind::ConfigDirectory, Some(Box::new(e)))
    }
}

impl From<toml::de::Error> for Error {
    fn from(e: toml::de::Error) -> Error {
        Error::new(ErrorKind::ConfigReading, Some(Box::new(e)))
    }
}

impl From<toml::ser::Error> for Error {
    fn from(e: toml::ser::Error) -> Error {
        Error::new(ErrorKind::ConfigWriting, Some(Box::new(e)))
    }
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Error {
        Error::new(ErrorKind::HTTPRequest, Some(Box::new(e)))
    }
}

pub struct DisplayabalePath(PathBuf);

impl fmt::Display for DisplayabalePath {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0.display())
    }
}

pub trait ResultExt<T, E>: failure::ResultExt<T, E>
where
    E: fmt::Display,
{
    fn with_path_context<P: AsRef<Path>>(
        self,
        p: P,
    ) -> result::Result<T, failure::Context<DisplayabalePath>>
    where
        Self: Sized,
    {
        self.context(DisplayabalePath(p.as_ref().to_path_buf()))
    }
}

impl<T, E: fmt::Display> ResultExt<T, E> for result::Result<T, E>
where
    result::Result<T, E>: failure::ResultExt<T, E>,
{
}

pub fn format_causes<F: Fail>(failure: &F) -> String {
    let mut causes = failure.causes();
    if let Some(first) = causes.next() {
        causes.fold(first.to_string(), |acc, x| {
            format!("{}{}{}", acc, g_(": "), x)
        })
    } else {
        "".to_string()
    }
}
