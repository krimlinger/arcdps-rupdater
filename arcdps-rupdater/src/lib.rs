extern crate failure;
extern crate gettextrs;
#[macro_use]
extern crate failure_derive;
#[macro_use]
extern crate serde_derive;

pub mod config;
pub mod downloader;
pub mod error;
pub mod globals;
pub mod hash;
pub mod updater;

use std::path::{Path, PathBuf};

pub fn setup_textdomain<T: AsRef<Path>>(program_path: T) {
    // Try to find LOCALEDIR relatively to program_path if building
    // for Windows (where the LOCALEDIR is most likely wrong because of
    // installdir being set during the installer execution, when the
    // program is already compiled).
    if cfg!(target_os = "windows") {
        let mut textdomain_dir = PathBuf::from(program_path.as_ref());
        textdomain_dir.pop();
        textdomain_dir.pop();
        textdomain_dir.push("share");
        textdomain_dir.push("locale");
        gettextrs::bindtextdomain("arcdps-rupdater", &format!("{}", textdomain_dir.display()));
    } else {
        gettextrs::bindtextdomain("arcdps-rupdater", globals::LOCALEDIR);
    }

    gettextrs::bind_textdomain_codeset("arcdps-rupdater", "UTF-8");
    gettextrs::textdomain("arcdps-rupdater");
}
