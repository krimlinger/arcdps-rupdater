extern crate app_dirs2;
extern crate serde;
extern crate toml;

use std::collections::hash_map::RandomState;
use std::collections::hash_set::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

use self::app_dirs2::{app_root, AppDataType, AppInfo};
use self::serde::de::DeserializeOwned;
use self::serde::ser::Serialize;

use error::{Result, ResultExt};
use hash;

const APP_INFO: AppInfo = AppInfo {
    name: "arcdps-rupdater",
    author: "Arcdps Rupdater Author",
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GenConfig<T, U> {
    #[serde(default)]
    pub gw2_dir: T,
    #[serde(default)]
    pub gw2_exec: U,
    #[serde(default)]
    pub blacklist: HashSet<String, RandomState>,
}

pub type OptionConfig = GenConfig<Option<PathBuf>, Option<bool>>;

pub type Config = GenConfig<PathBuf, bool>;

fn toml_from_reader<R, T>(reader: R) -> Result<T>
where
    R: Read,
    T: DeserializeOwned,
{
    let mut buffer: Vec<u8> = Vec::new();
    // Todo: fix mut.
    let mut r = reader;
    r.read_to_end(&mut buffer)?;
    Ok(toml::from_slice(&buffer)?)
}

fn toml_to_writer<W, T>(writer: W, value: &T) -> Result<()>
where
    W: Write,
    T: Serialize,
{
    // Todo: fix mut and pretty TOML.
    //let serialized_value = toml::to_string_pretty(value)?.into_bytes();
    let serialized_value = toml::to_vec(value)?;
    let mut w = writer;
    Ok(w.write_all(&serialized_value)?)
}

impl OptionConfig {
    pub fn new() -> OptionConfig {
        OptionConfig {
            gw2_dir: None,
            gw2_exec: None,
            blacklist: HashSet::new(),
        }
    }
}

impl Config {
    pub fn new() -> Config {
        Config {
            gw2_dir: PathBuf::from("C:\\Program Files (x86)\\Guild Wars 2"),
            gw2_exec: false,
            blacklist: HashSet::new(),
        }
    }

    pub fn with_gw2_dir<T: AsRef<Path>>(&mut self, gw2_dir: T) -> Config {
        Config {
            gw2_dir: gw2_dir.as_ref().into(),
            gw2_exec: self.gw2_exec,
            blacklist: self.blacklist.clone(),
        }
    }

    pub fn with_gw2_exec(&mut self, gw2_exec: bool) -> Config {
        Config {
            gw2_dir: self.gw2_dir.clone(),
            gw2_exec: gw2_exec,
            blacklist: self.blacklist.clone(),
        }
    }

    pub fn with_blacklist(&mut self, blacklist: HashSet<String, RandomState>) -> Config {
        Config {
            gw2_dir: self.gw2_dir.clone(),
            gw2_exec: self.gw2_exec,
            blacklist: blacklist,
        }
    }

    pub fn add_to_blacklist(&mut self, md5_sum: &str) -> bool {
        self.blacklist.insert(md5_sum.to_string())
    }

    pub fn remove_from_blacklist(&mut self, md5_sum: &str) -> bool {
        self.blacklist.remove(md5_sum)
    }

    pub fn load_optionconfig(&mut self, oc: OptionConfig) {
        if let Some(gw2_dir) = oc.gw2_dir {
            self.gw2_dir = gw2_dir;
        }
        self.gw2_exec = oc.gw2_exec.unwrap_or(self.gw2_exec);
    }
}

pub fn get_config_directory() -> Result<PathBuf> {
    Ok(app_root(AppDataType::UserConfig, &APP_INFO)?)
}

pub fn get_config_path() -> Result<PathBuf> {
    let app_root = get_config_directory()?;
    Ok(app_root.join("config.toml"))
}

pub fn read_from_filesystem() -> Result<Config> {
    let config_path = get_config_path()?;
    let config_file = File::open(&config_path).with_path_context(&config_path)?;
    let mut config: Config = toml_from_reader(config_file).with_path_context(&config_path)?;
    config
        .blacklist
        .retain(|md5_sum| hash::hex_str_to_digest(md5_sum).is_ok());
    Ok(config)
}

pub fn write_to_filesystem(config: &Config) -> Result<()> {
    let config_path = get_config_path()?;
    let config_file = File::create(&config_path).with_path_context(&config_path)?;
    Ok(toml_to_writer(config_file, &config).with_path_context(&config_path)?)
}

pub fn create_default_config() -> Result<Config> {
    let config = Config::new();
    write_to_filesystem(&config)?;
    Ok(config)
}
