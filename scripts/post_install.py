#!/usr/bin/python3
# coding: utf-8

import os
from pathlib import Path
from subprocess import call

prefix = os.fspath(os.environ.get('MESON_INSTALL_PREFIX'))
if prefix is None:
    raise RuntimeError('MESON_INSTALL_PREFIX environemnt variable is unset.')

datadir = Path(prefix, 'share')

# Delegate this work to package manager and install scripts
# if DESTIDIR exists.
if os.environ.get('DESTDIR') is None:
    print('Updating icon cache...')
    call(['gtk-update-icon-cache', '-qtf', Path(datadir, 'icons', 'hicolor')])
    print('Updating desktop database...')
    call(['update-desktop-database', '-q', Path(datadir, 'applications')])
