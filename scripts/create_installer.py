#!/usr/bin/env python3
# coding: utf-8

import os
import shutil
import subprocess

from pathlib import Path


prefix = os.fspath(os.environ.get('MESON_INSTALL_PREFIX'))
root = os.fspath(os.environ.get('MESON_SOURCE_ROOT'))

if prefix is None:
    raise RuntimeError('MESON_INSTALL_PREFIX environemnt variable is unset.')

if root is None:
    raise RuntimeError('MESON_SOURCE_ROOT environemnt variable is unset.')

install_prefix = Path(prefix)
source_root = Path(root)
installer_script = source_root / 'scripts' / 'create_installer.nsis'
installer = install_prefix / 'arcdps-rupdater-installer.exe'

subprocess.run(['makensis', '-NOCD', installer_script],
               cwd=install_prefix)

# Use shutil.move instead of Path.rename to avoid cross-device link error.
shutil.move(os.fspath(installer),
            os.fspath(source_root / 'arcdps-rupdater-installer.exe'))
