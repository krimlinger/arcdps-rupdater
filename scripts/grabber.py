#!/usr/bin/env python3
# coding: utf-8

import itertools
from pathlib import Path

grab_dirs = ('arcdps-rupdater', 'arcdps-rupdater-gui')

for p in itertools.chain(*(Path(d).rglob('*.rs') for d in grab_dirs)):
    print(p)
