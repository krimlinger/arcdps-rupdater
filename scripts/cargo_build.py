#!/usr/bin/env python3
# coding: utf-8

import argparse
import os
import shutil
import subprocess
import sys

from pathlib import Path


parser = argparse.ArgumentParser(description='Custom cargo build target for meson')
parser.add_argument('--target', help='optional cargo target to build for')
parser.add_argument('--release', action='store_true',
                    help='make cargo build in release mode')
parser.add_argument('--cross-compile', action='store_true',
                    help='make cargo use a cross-compilation toolchain')
parser.add_argument('--cross-toolchain-root',
                    help='root of the cross compilation toolchain')
parser.add_argument('src', help='source directory where cargo should be run')
parser.add_argument('dest', 
    help='output directory where generated executables should be copied')
parser.add_argument('localedir', help='path to localisation data')
args = parser.parse_args()

optional_args = []
profile = 'debug'
src = Path(args.src)
dest = Path(args.dest)
manifest = src / 'Cargo.toml'
binary_src = src / 'target'
binary_ext = ''

if args.release:
    profile = 'release'
    optional_args += ['--release']

if args.target:
    optional_args += ['--target', args.target]
    binary_src = binary_src / args.target / profile
    if 'windows' in args.target:
        binary_ext = '.exe'
else:
    binary_src = binary_src / profile

binaries = (binary_src / ('arcdps-rupdater' + b + binary_ext) for b in ('', '-gui'))

modified_env = os.environ.copy()
modified_env['ARCDPS_RUPDATER_LOCALEDIR'] = os.fspath(args.localedir)

if args.cross_compile:
    modified_env['PKG_CONFIG_ALLOW_CROSS'] = "1"
    if args.cross_toolchain_root:
        modified_env['PKG_CONFIG_PATH'] = Path(args.cross_toolchain_root, 
                                               'lib', 'pkgconfig')

subprocess.Popen(
        ['cargo', 'build', '--manifest-path', manifest] + optional_args,
        env=modified_env)

for b in binaries:
    shutil.copy(b, dest / b.name)
